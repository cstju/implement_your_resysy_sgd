# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-09-14 10:08:25
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-09-14 11:14:17
import numpy as np
import pandas as pd
from sklearn import cross_validation as cv
import matplotlib.pyplot as plt
from matplotlib import *

data_file = "/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u.data"

header = ['user_id', 'item_id', 'rating', 'timestamp']
df = pd.read_csv(data_file, sep='\t', names=header)
n_users = df.user_id.unique().shape[0]
n_items = df.item_id.unique().shape[0]

print 'Number of users = ' + str(n_users) + ' | Number of movies = ' + str(n_items)


train_data, test_data = cv.train_test_split(df,test_size=0.25)
#train_data = pd.DataFrame(train_data)
#test_data = pd.DataFrame(test_data)

# Create training and test matrix
train_data_matrix = np.zeros((n_users, n_items))
for line in train_data.itertuples():
    train_data_matrix[line[1]-1, line[2]-1] = line[3]  

test_data_matrix = np.zeros((n_users, n_items))
for line in test_data.itertuples():
    test_data_matrix[line[1]-1, line[2]-1] = line[3]

#这里构建二值矩阵的目的仅仅是为了计算rmse时的矩阵运算方便
    
# Index matrix for training data
train_bool_matrix = train_data_matrix.copy()
train_bool_matrix[train_bool_matrix > 0] = 1
train_bool_matrix[train_bool_matrix == 0] = 0

# Index matrix for test data
test_bool_matrix = test_data_matrix.copy()
test_bool_matrix[test_bool_matrix > 0] = 1
test_bool_matrix[test_bool_matrix == 0] = 0

# Predict the unknown ratings through the dot product of the latent features for users and items 
def prediction(ufm,ifm):
    return np.dot(ufm.T,ifm)

lmbda = 0.1 # Regularisation weight
k = 20  # Dimension of the latent feature space
m, n = train_data_matrix.shape  # Number of users and items
n_epochs = 3  # Number of epochs
gamma=0.01  # Learning rate

ufm = 3 * np.random.rand(k,m) # Latent user feature matrix
ifm = 3 * np.random.rand(k,n) # Latent movie feature matrix

# Calculate the RMSE
def rmse(bool_matrix,data_matrix,ufm,ifm):
    return np.sqrt(np.sum((bool_matrix * (data_matrix - prediction(ufm,ifm)))**2)/len(data_matrix[data_matrix > 0]))

train_errors = []
test_errors = []
"""
zip([seql, ...])接受一系列可迭代对象作为参数，将对象中对应的元素打包成一个个tuple（元组），
然后返回由这些tuples组成的list（列表）。若传入参数的长度不等，则返回list的长度和参数中长度最短的对象相同。
x = [1, 2, 3]
y = [4, 5, 6]
z = [7, 8, 9]
xyz = zip(x, y, z)
print xyz
>>>[(1, 4, 7), (2, 5, 8), (3, 6, 9)]
print type(xyz)
>>><type 'list'>
print xyz[0]
>>>(1, 4, 7)
print type(xyz[0])
>>><type 'tuple'>
x = [1, 2, 3]
y = [4, 5, 6, 7]
xy = zip(x, y)
print xy
>>>[(1, 4), (2, 5), (3, 6)]
"""
"""
>>> name=('jack','beginman','sony','pcky')
>>> age=(2001,2003,2005,2000)
>>> for a,n in zip(name,age):
    print a,n

输出：
jack 2001
beginman 2003
sony 2005
pcky 2000
"""
#Only consider non-zero matrix 
print 'begin epochs'
users,items = train_data_matrix.nonzero()      
for epoch in xrange(n_epochs):
    for u, i in zip(users,items):
        e = train_data_matrix[u, i] - prediction(ufm[:,u],ifm[:,i])  # Calculate error for gradient
        ufm[:,u] += gamma * ( e * ifm[:,i] - lmbda * ufm[:,u]) # Update latent user feature matrix
        ifm[:,i] += gamma * ( e * ufm[:,u] - lmbda * ifm[:,i])  # Update latent movie feature matrix
    train_rmse = rmse(train_bool_matrix,train_data_matrix,ufm,ifm) # Calculate root mean squared error from train dataset
    test_rmse = rmse(test_bool_matrix,test_data_matrix,ufm,ifm) # Calculate root mean squared error from test dataset
    train_errors.append(train_rmse)
    test_errors.append(test_rmse)
    
# Check performance by plotting train and test errors

plt.plot(range(n_epochs), train_errors, marker='o', label='Training Data');
plt.plot(range(n_epochs), test_errors, marker='v', label='Test Data');
plt.title('SGD-WR Learning Curve')
plt.xlabel('Number of Epochs');
plt.ylabel('RMSE');
plt.legend()
plt.grid()
plt.show()

# Calculate prediction matrix train_data_hat (low-rank approximation for train_data_matrix)
train_data = pd.DataFrame(train_data_matrix)
train_data_hat=pd.DataFrame(prediction(ufm,ifm))

# Compare true ratings of user 17 with predictions
ratings = pd.DataFrame(data=train_data.loc[16,train_data.loc[16,:] > 0]).head(n=5)
ratings['Prediction'] = train_data_hat.loc[16,train_data.loc[16,:] > 0]
ratings.columns = ['Actual Rating', 'Predicted Rating']
print ratings