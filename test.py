# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-09-14 11:06:26
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-09-14 11:10:29
x = [1, 2, 3]
y = [4, 5, 6]
z = [7, 8, 9]
xyz = zip(x, y, z)
print xyz
print type(xyz)
print xyz[0]
print type(xyz[0])
x = [1, 2, 3]
y = [4, 5, 6, 7]
xy = zip(x, y)
print xy